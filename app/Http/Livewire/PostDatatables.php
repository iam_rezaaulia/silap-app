<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostDatatables extends Component
{
    use WithPagination;

    public $headers;

    private function headerConfig()
    {
        return [
            'id' => 'No.',
            'body' => 'Kegiatan',
            'quantity' => 'Kuantitas',
            'unit' => 'Satuan',
            'created_at' => 'Tanggal',
        ];
    }

    public function mount()
    {
        $this->headers = $this->headerConfig();
    }

    private function resultData()
    {
        return Post::where('user_id', '=', Auth::id())->paginate(10);
    }

    public function render()
    {
        return view('livewire.post-datatables', [
            'data' => $this->resultData(),
        ]);
    }
}
