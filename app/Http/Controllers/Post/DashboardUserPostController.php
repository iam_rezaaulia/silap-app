<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardUserPostController extends Controller
{
    public function index(User $user)
    {
        $posts = $user->posts()->latest()->with(['user', 'likes'])->paginate(10);
        
        return view('posts.user', [
            'user' => $user,
            'posts' => $posts,
        ]);
    }
}
