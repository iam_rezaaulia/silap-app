<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Mail\SendingEmailPostLiked;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PostLikeController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }
    
    public function store(Post $post, Request $request)
    {
        if ($post->likeBy($request->user())) {
            return response('Anda sudah like postingan ini', 409);
        }

        $post->likes()->create([
            'user_id' => $request->user()->id,
        ]);

        $user = auth()->user();

        if (! $post->likes()->onlyTrashed()->where('user_id', $request->user()->id)->count()) {
            Mail::to($post->user)->send(new SendingEmailPostLiked($user, $post));
        }

        return back();
    }

    public function destroy(Post $post, Request $request)
    {
        $request->user()->likes()->where('post_id', $post->id)->delete();

        return back();
    }
}
