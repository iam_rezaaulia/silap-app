@props(['post' => $post])

<div class='bg-gray-100 px-4 py-2 rounded-lg my-4 flex flex-col'>
    <div class='flex justify-between mb-4'>
        <div>
            <a href={{ route('userPost', $post->user) }} class='font-medium'>{{ Str::ucfirst($post->user->name) }}</a> 
            <span class='text-gray-400 text-sm'>{{ $post->created_at->diffForHumans() }}</span>
        </div>
        @can('delete', $post)
            <form action={{ route('deletePost', $post) }} method="POST">
                @csrf
                @method('DELETE')
                <button type='submit' class='text-gray-400 focus:outline-none'>Delete</button>
            </form>
        @endcan
    </div>
    <a href="{{ route('showPost', $post) }}" class="mb-2 font-normal">{{ $post->body }}</a>
    
    <div class="flex mb-2">
        <div>
            <span class="relative inline-block px-3 py-1 font-light text-green-900 leading-tight">
                <span aria-hidden class="absolute inset-0 bg-green-200 opacity-50 rounded-sm"></span>
                <span class="relative">{{ $post->quantity }}</span>
            </span>
        </div>
        <div class="ml-2">
            <span class="relative inline-block px-3 py-1 font-light text-green-900 leading-tight">
                <span aria-hidden class="absolute inset-0 bg-green-200 opacity-50 rounded-sm"></span>
                <span class="relative">{{ $post->unit }}</span>
            </span>
        </div>
    </div>
    
    

    <div class='flex items-center'>
        @auth
            @if (! $post->likeBy(auth()->user()))
                <form action={{ route('likePost', $post) }} method="POST" class='mr-2'>
                @csrf
                <button type='submit' class='text-blue-500 font-light focus:outline-none'>Like</button>
                </form>
            @else
            <form action={{ route('unlikePost', $post) }} method="POST" class='mr-2'>
                @csrf
                @method('DELETE')
                <button type='submit' class='text-blue-500 font-light focus:outline-none'>Unlike</button>
            </form>
            @endif
        @endauth
        <span class="font-light">
            {{ $post->likes->count() }}  {{ Str::plural('like', $post->likes->count()) }}
        </span>
    </div>
</div>