@extends('layouts.auth')

@section('title', 'Login Page')

@section('content')
<div class="w-full flex flex-wrap">
  <!-- Component -->
  <div class="w-full md:w-1/2 flex flex-col">
    <div class="flex justify-center md:justify-start pt-12 md:pl-12 md:-mb-24">
      <a href="{{ route('home') }}" class="bg-gray-900 text-white text-lg p-4">Beranda</a>
    </div>

    <div class="flex flex-col justify-center md:justify-start my-auto pt-8 md:pt-0 px-8 md:px-24 lg:px-32">
      <p class="text-gray-900 text-center text-3xl font-medium">Selamat Datang Kembali</p>
      <form class="flex flex-col pt-3 md:pt-8" action={{ route('login') }} method="POST">
        @csrf
        <div class='flex flex-col pt-4'>
          <label for='email' class='text-base'>Email</label>
          <input type='email' name='email' id='email' placeholder='Email' 
          class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline @error('email') border-red-500 @enderror" value={{ old('email') }}>

          @error('email')
            <div class='text-red-500 mt-2 text-sm'>
              {{ $message }}
            </div>
          @enderror
        </div>
        <div class='flex flex-col pt-4'>
          <label for='password' class='text-base'>Password</label>
          <input type='password' name='password' id='password' placeholder='Password'
          class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mt-1 leading-tight focus:outline-none focus:shadow-outline @error('password') border-red-500 @enderror" value={{ old('password') }}>

          @error('password')
            <div class='text-red-500 mt-2 text-sm'>
              {{ $message }}
            </div>
          @enderror
        </div>

        <div class='mt-8'>
          <button type='submit' class='w-full bg-gray-900 text-white text-lg hover:bg-gray-800 p-2 mt-8 focus:outline-none'> 
            Masuk
          </button>
        </div>
      </form>
      <div class="text-center pt-12 pb-12">
        <p>Belum punya akun ? <a href="{{ route('register') }}" class="hover:text-gray-800 underline font-semibold">Daftar disini</a></p>
      </div>
    </div>
  </div>

  <!-- Image -->
  <div class="w-1/2 shadow-2xl">
      <img class="object-cover w-full h-screen hidden md:block" src="https://source.unsplash.com/guNIjIuUcgY"> 
  </div>
</div>
@endsection