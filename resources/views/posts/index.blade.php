@extends('layouts.app')

@section('content')
    <div class='bg-white flex justify-center py-8'>
      <div class='rounded-lg mb-4'>
        <form action={{ route('posts') }} method="POST" class='mb-8 flex flex-col'>
          @csrf
          <div class='mb-4'>
            <label for='body' class='sr-only'>Text</label>
            <textarea name='body' id='body' cols='30' rows='4'
            class="bg-gray-100 border w-full p-4 focus:outline-none focus:shadow-outline rounded-lg @error('body') border-red-500 @enderror" placeholder="Apa kegiatan kamu hari ini ?"></textarea>

            @error('body')
              <div class='text-red-500 mt-2 text-sm'>
                {{ $message }}
              </div>
            @enderror
          </div>
          <div class="flex justify-between mb-4">
            <div class="flex items-center">
              <div>
                <label for="quantity" class="mr-2">Jumlah</label>
                <input type='quantity' name='quantity' id='quantity' placeholder='Jumlah' 
                class="bg-gray-100 border w-24 rounded py-2 px-3 text-center leading-tight focus:outline-none focus:shadow-outline @error('quantity') border-red-500 @enderror" value={{ old('quantity') }}>

                @error('quantity')
                  <div class='text-red-500 mt-2 text-sm'>
                    {{ $message }}
                  </div>
                @enderror
              </div>
              <div class="ml-6">
                <label for="unit" class="mr-2">Satuan</label>
                <input type='unit' name='unit' id='unit' placeholder='Ex: Surat, Laporan, File dll' 
                class="bg-gray-100 border rounded py-2 px-3 leading-tight focus:outline-none focus:shadow-outline @error('unit') border-red-500 @enderror" value={{ old('unit') }}>

                @error('unit')
                  <div class='text-red-500 mt-2 text-sm'>
                    {{ $message }}
                  </div>
                @enderror
              </div>
            </div>
            <div>
              <button type='submit' class='bg-blue-500 text-white px-6 py-2 rounded font-normal focus:outline-none'>
                Posting 
              </button>
            </div>
          </div>
        </form>
        
        @if ($posts->count())
          @foreach ($posts as $post)
            <x-post :post='$post' />
          @endforeach

          {{ $posts->links() }}
        @else
          <p class='font-medium text-center'>Belum ada kegiatan.</p>
        @endif
      </div>
    </div>
@endsection