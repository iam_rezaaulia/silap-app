@extends('layouts.app')

@section('content')
  <div class='bg-white flex justify-center py-8'>
    <div class='rounded-lg mb-4'>
      <x-post :post='$post' />
    </div>
  </div>
@endsection