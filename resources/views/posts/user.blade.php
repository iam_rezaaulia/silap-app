@extends('layouts.app')

@section('content')
  <div class='bg-white flex justify-center py-8'>
    <div class='rounded-lg mb-4'>
      <p class='font-semibold'>{{ Str::ucfirst($user->name) }}</p>
      <p class='text-gray-400'>Total {{ $posts->count() }} kegiatan</p>

      @if ($posts->count())
        @foreach ($posts as $post)
          <x-post :post='$post' />
        @endforeach

        {{ $posts->links() }}
      @else
        <p class='font-normal text-center'>{{ Str::ucfirst($user->name) }} Belum ada postingan kegiatan!</p>
      @endif
    </div>
  </div>
@endsection