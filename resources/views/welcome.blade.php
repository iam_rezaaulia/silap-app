@extends('layouts.app')

@section('content')
  <div class='py-8 bg-white'>
    <div class="container py-3 mx-auto flex flex-wrap flex-col md:flex-row items-center">
      <!--Left Col-->
      <div class="flex flex-col w-full md:w-2/5 justify-center items-start text-center md:text-left text-gray-800">
        <h1 class="my-4 text-4xl font-bold leading-tight">
          Aplikasi Laporan PPNPN Online
        </h1>
        <p class="leading-normal text-1xl mb-8">
          Sampaikan kegiatan sehari-hari Anda di sini.
        </p>
        <button
          class="mx-auto lg:mx-0 bg-blue-500 text-white rounded-md my-6 py-4 px-8 shadow-lg focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-100 ease-in-out">
          <a href="{{ route('login')}}">Posting Kegiatan ! </a>
        </button>
      </div>
      <!--Right Col-->
      <div class="w-full md:w-3/5 text-center">
        <img class="object-fill px-12 transform transition hover:scale-95 duration-300 ease-in-out"
          src="{{ asset('images/hero.svg') }}" />
      </div>
    </div>
  </div>
@endsection