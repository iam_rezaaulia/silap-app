<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset='UTF-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
  <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/x-icon"/>
  <link href='{{ asset('css/app.css') }}' rel='stylesheet'>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <style>
    html, body {
      font-family: 'Poppins', sans-serif;
      font-style: 'normal';
      scroll-behavior: smooth;
    }
  </style>
  <title>SILAP - Kanwil BPN Aceh</title>
</head>
<body class="bg-white leading-normal tracking-normal flex flex-col min-h-screen">
  <nav class="flex items-center justify-between py-6 bg-white px-28">
    <div class="flex items-center">
      <a class="font-bold tracking-widest text-3xl text-blue-500 mr-6" href="{{ route('home') }}">SILAP</a>
      <a href={{ route('posts') }} class='p-3'>Kegiatan</a>
      @auth
        <a href={{ route('dashboard') }} class='p-3'>Dashboard</a>
      @endauth
    </div>
    <div class='flex items-center'>
      @auth
        <form action={{ route('logout') }} method="POST" class='p-3 inline'>
          @csrf
          <button type='submit' class='focus:outline-none'>Logout</button>
        </form>
        <div class="text-black font-medium"> {{ Str::ucfirst(auth()->user()->name) }} </div>
      @endauth
      @guest
        <button class="text-black font-normal rounded-md py-3 border-black px-4 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-100 ease-in-out">
          <a href={{ route('login') }} class='p-3'>Masuk</a>
        </button>
        <button class="bg-transparent text-blue-500 rounded-md py-2 px-4 border-2 border-blue-500 focus:outline-none focus:shadow-outline transform transition hover:scale-105 duration-100 ease-in-out">
          <a href={{ route('register') }} class='p-3'>Daftar</a>
        </button>
      @endguest
    </div>
  </nav>
  <main class="flex-grow">
    @yield('content')
  </main>
  <footer class="bg-white flex items-center justify-between py-6 px-20">
    <p class="flex items-center">Kanwil BPN Aceh &copy All rights reserved</p>
    <div class="flex items-center">
      <svg fill="#e53e3e" viewBox="0 0 24 24"  class="w-5 h-5 mx-1 pt-px text-red-600" stroke="currentColor">
        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"/>
      </svg>
      <p class="">Tim Bagian Kepegawaian</p>
    </div>
    <div class="flex items-center">
      <a href="https://www.youtube.com/channel/UCucGV87aTWzYGgicgMe1NKQ" target="_blank">
        <img class="w-6 h-6 transform transition hover:scale-105 duration-100 ease-in-out"
        src="{{ asset('images/youtube.svg') }}" />
      </a>
      <a href="https://www.instagram.com/kanwilbpnaceh/" class="ml-6" target="_blank">
        <img class="w-6 h-6 transform transition hover:scale-105 duration-100 ease-in-out"
        src="{{ asset('images/instagram.svg') }}" />
      </a>
      <a href="https://open.spotify.com/show/6CUvwzaAiDo6fXGd0zEvkY" class="ml-6" target="_blank">
        <img class="w-6 h-6 transform transition hover:scale-105 duration-100 ease-in-out"
        src="{{ asset('images/spotify.svg') }}" />
      </a>
      <a href="https://www.facebook.com/atrbpn.aceh.7" class="ml-6" target="_blank">
        <img class="w-6 h-6 transform transition hover:scale-105 duration-100 ease-in-out"
        src="{{ asset('images/facebook.svg') }}" />
      </a>
      <a href="https://twitter.com/KanwilBPNAceh" class="ml-6" target="_blank">
        <img class="w-6 h-6 transform transition hover:scale-105 duration-100 ease-in-out"
        src="{{ asset('images/twitter.svg') }}" />
      </a>
    </div>
  </footer>
  @include('sweetalert::alert')
  @livewireScripts
</body>
</html>