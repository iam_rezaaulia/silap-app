<!DOCTYPE html>
<html lang='en'>
<head>
  <meta charset='UTF-8'>
  <meta name='viewport' content='width=device-width, initial-scale=1.0'>
  <link rel="icon" href="{{ URL::asset('images/favicon.png') }}" type="image/x-icon"/>
  <link href='{{ asset('css/app.css') }}' rel='stylesheet'>
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
  <style>
    html, body {
      font-family: 'Poppins', sans-serif;
      font-style: 'normal';
      scroll-behavior: smooth;
    }
  </style>
  <title>
    @yield('title')
  </title>
  @livewireStyles
</head>
<body class='bg-gray-100'>
  @yield('content')
  @livewireScripts
</body>
</html>