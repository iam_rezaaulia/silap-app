<div class="antialiased font-sans bg-gray-100">
    <div class="container mx-auto px-4 sm:px-8">
        <div class="py-8">
            <div>
                <h2 class="text-xl font-semibold leading-tight">{{ Str::ucfirst(auth()->user()->name) }}</h2>
            </div>
            <div class="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                <div class="container inline-block min-w-full shadow rounded-lg overflow-hidden">
                    <table class="min-w-full leading-normal">
                        <thead>
                            <tr>
                                @foreach ($headers as $key => $value)
                                    <th class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-semibold text-gray-600 uppercase tracking-wider">
                                        {{ $value }}
                                    </th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @if ($data->count())
                                @foreach ($data as $item)
                                    <tr>
                                        @foreach ($headers as $key => $value)
                                            <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                                {{ $item->$key }}
                                            </td>
                                        @endforeach
                                    </tr>
                                @endforeach
                            @else
                                
                            @endif
                        </tbody>
                    </table>
                    
                    <div class="px-5 py-3 border-b-2 border-gray-200 bg-gray-100">
                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>