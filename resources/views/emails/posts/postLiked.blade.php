@component('mail::message')
# Postingan Anda telah disukai

{{ Str::ucfirst($liker->name) }} menyukai postingan Anda.

@component('mail::button', ['url' => route('showPost', $post)])
  Lihat postingan
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
