<?php

use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Pages\DashboardController;
use App\Http\Controllers\Pages\HomeController;
use App\Http\Controllers\Post\PostController;
use App\Http\Controllers\Post\PostLikeController;
use App\Http\Controllers\Post\DashboardUserPostController;
use Illuminate\Support\Facades\Route;

Route::get('/', [HomeController::class, 'index'])->name('home');

Route::get('/dashboard', DashboardController::class)->name('dashboard')->middleware('auth');

// Post
Route::get('/posts', [PostController::class, 'index'])->name('posts');
Route::get('/{user:username}/posts', [DashboardUserPostController::class, 'index'])->name('userPost');
Route::get('/posts/{post}', [PostController::class, 'show'])->name('showPost');
Route::post('/posts', [PostController::class, 'store']);
Route::delete('/posts/{post}', [PostController::class, 'destroy'])->name('deletePost');

Route::post('/posts/{post}/likes', [PostLikeController::class, 'store'])->name('likePost');
Route::delete('/posts/{post}/likes', [PostLikeController::class, 'destroy'])->name('unlikePost');

// Authentication
Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class, 'store']);
Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class, 'store']);
Route::post('/logout', [LogoutController::class, 'store'])->name('logout');
